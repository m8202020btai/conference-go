import json
import requests
from .keys import PEXELS_API_KEY  # , OPEN_WEATHER_API_KEY


def get_photo(city, state):
    # Use the Pexels API
    url = "https://api.pexels.com/v1/search"
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)

    return {"picture_url": content["photos"][0]["src"]["original"]}


def get_weather_data(city, state):
    # Use the Open Weather API
    pass
